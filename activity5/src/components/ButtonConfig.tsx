import { useContext } from "react";
import { TouchableOpacity, Text } from "react-native";
import { ThemeContext } from "../styles/DefaultTheme";
import { Styles } from "../styles/GlobalStyles";

interface ButtonProps{
    onPress: () => void;
    title: string;
    isBlue?: boolean;
    isGray?: boolean;
}

export default function Button({title, onPress, isBlue, isGray}: ButtonProps) {
    const theme = useContext(ThemeContext);
    return(
        <TouchableOpacity
            style={
                isBlue
                ? Styles.btndarkbrown
                : isGray
                ? Styles.btnLbrown
                : theme === "light"
                ? Styles.btnLight
                : Styles.btnColorDark
            }
            onPress={onPress}>
            <Text
                style={
                    isBlue || isGray
                    ? Styles.smallTextLight
                    : theme === "dark"
                    ? Styles.smallTextLight
                    : Styles.smallTextDark
                }
            >
                {title}
            </Text>
        </TouchableOpacity>
    )
}