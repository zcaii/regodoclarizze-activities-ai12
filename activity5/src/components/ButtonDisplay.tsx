import * as React from "react";
import Button from "./ButtonConfig";
import { View, Text } from "react-native";
import { Styles } from "../styles/GlobalStyles";
import { ThemeColors } from "../styles/ThemeColors";
import { evaluate} from "mathjs";

export default function ButtonDisplay() {
    
  const [lowerDisplay, setLowerDisplay] = React.useState("");
  const [upperDisplay, setUpperDisplay] = React.useState("");
  const [operationDisplay, setOperationDisplay] = React.useState("");
  const [number, setNumbers] = React.useState<Number[]>([]);
  const [operators, setOperators] = React.useState<string[]>([]);
  const [total, setTotal] = React.useState<Number | null >(null);

  const numPressHandler = (buttonValue: string) => {
    if (lowerDisplay.length < 10) {
      setLowerDisplay(lowerDisplay + buttonValue);
    }
  };

  const operatorPressHandler = (buttonPressed: string) => {
    setOperationDisplay(buttonPressed);
    setNumbers([...number, parseInt(lowerDisplay)]);
    setOperators([...operators, buttonPressed]);
    if(buttonPressed==="%"){
        setLowerDisplay(evaluate(lowerDisplay + "%").toString());
        setOperationDisplay("");
    } else{
        if(upperDisplay.length<1){
            setUpperDisplay(lowerDisplay + buttonPressed);
        } else {
            setUpperDisplay(upperDisplay + lowerDisplay + buttonPressed);
        }
        setLowerDisplay("");
        setOperationDisplay("");
    }
};
  const signHandler = () => {
      if(parseInt(lowerDisplay)<0){
          setLowerDisplay(lowerDisplay.slice(1));
      } else {
          setLowerDisplay("-" + lowerDisplay);
      }
  }

  const clearAll = () => {
    setLowerDisplay("");
    setUpperDisplay("");
    setOperationDisplay("");
    setTotal(null);
    setNumbers([]);
    setOperators([]);
  };

  const firstNumberDisplay = () => {
    if(total !== null) {
        return <Text style={total < 99999 ? [Styles.screenFirstNumber, {color: ThemeColors.total}] : [Styles.screenFirstNumber, {fontSize: 50, color: ThemeColors.total}]}>{total?.toString()}</Text>
    }
    if(lowerDisplay && lowerDisplay.length<6){
        return <Text style={Styles.screenFirstNumber}>{lowerDisplay}</Text>
    }
    if(lowerDisplay===""){
        return <Text style={Styles.screenFirstNumber}>{"0"}</Text>
    }
    if(lowerDisplay.length>5 && lowerDisplay.length<8){
        return(
            <Text style={[Styles.screenFirstNumber, {fontSize: 70}]}>
                {lowerDisplay}
            </Text>
        );
    }
    if(lowerDisplay.length > 7){
        return(
            <Text style={[Styles.screenFirstNumber, {fontSize: 30}]}>
                {lowerDisplay}
            </Text>
        );
    };
}

  const gettotal = () => {
    setLowerDisplay("");
    setUpperDisplay("");
    setLowerDisplay(evaluate(upperDisplay + lowerDisplay).toString());
};

  return (
    <View style={Styles.viewButton}>
      <View
        style={{
          height: 120,
          width: "90%",
          justifyContent: "flex-end",
          alignSelf: "center",
        }}
      >
        <Text style={Styles.screenSecondNumber}>
          {upperDisplay}
          <Text style={{ color: "purple", fontSize: 50, fontWeight: '500' }}>{operationDisplay}</Text>
        </Text>
        {firstNumberDisplay()}
      </View>
      <View style={Styles.row}>
        <Button title="C" isGray onPress={clearAll} />
        <Button title="+/-" isGray onPress={() => signHandler()} />
        <Button title="%" isGray onPress={() => operatorPressHandler
      ("%")} />
        <Button title="÷" isBlue onPress={() => operatorPressHandler
      ("/")} />
      </View>
      <View style={Styles.row}>
        <Button title="7" onPress={() => numPressHandler("7")} />
        <Button title="8" onPress={() => numPressHandler("8")} />
        <Button title="9" onPress={() => numPressHandler("9")} />
        <Button title="×" isBlue onPress={() => operatorPressHandler
      ("*")} />
      </View>
      <View style={Styles.row}>
        <Button title="4" onPress={() => numPressHandler("4")} />
        <Button title="5" onPress={() => numPressHandler("5")} />
        <Button title="6" onPress={() => numPressHandler("6")} />
        <Button title="-" isBlue onPress={() => operatorPressHandler
      ("-")} />
      </View>
      <View style={Styles.row}>
        <Button title="1" onPress={() => numPressHandler("1")} />
        <Button title="2" onPress={() => numPressHandler("2")} />
        <Button title="3" onPress={() => numPressHandler("3")} />
        <Button title="+" isBlue onPress={() => operatorPressHandler
      ("+")} />
      </View>
      <View style={Styles.row}>
        <Button title="." onPress={() => numPressHandler(".")} />
        <Button title="0" onPress={() => numPressHandler("0")} />
        <Button title="⌫" onPress={() => setLowerDisplay(lowerDisplay.slice(0, -1))} />
        <Button title="=" isBlue onPress={() => gettotal()} />
      </View>
    </View>
  );
}