import { StyleSheet } from "react-native";
import { ThemeColors } from "./ThemeColors";

export const Styles = StyleSheet.create({
    btndarkbrown: {
        width: 72,
        height: 72,
        borderRadius: 50,
        backgroundColor: ThemeColors.darkbrown,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 8,
    },
    btnColorDark: {
        width: 72,
        height: 72,
        borderRadius: 50,
        backgroundColor: ThemeColors.btnColorDark,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 8,
    },
    btnLight: {
        width: 72,
        height: 72,
        borderRadius: 50,
        backgroundColor: ThemeColors.numBG,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 8,
    },
    btnLbrown: {
        width: 72,
        height: 72,
        borderRadius: 50,
        backgroundColor: ThemeColors.btnLbrown,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 8,
    },
    smallTextLight: {
        fontSize: 32,
        color: ThemeColors.numBG,
    },
    smallTextDark: {
        fontSize: 32,
        color: ThemeColors.black,
    },

    //Keyboard
    row: {
        maxWidth: '100%',
        flexDirection: 'row',
    },
    viewButton: {
        position: 'absolute',
        bottom: 50,
    },
    screenFirstNumber: {
        fontSize: 96,
        color: ThemeColors.gray,
        fontWeight: '200',
        alignSelf: 'flex-end',
    },
    screenSecondNumber: {
        fontSize: 40,
        color: ThemeColors.gray,
        fontWeight: '200',
        alignSelf: 'flex-end',
    },
});