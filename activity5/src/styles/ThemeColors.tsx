export const ThemeColors = {
    light: '#f5ede4', //background
    dark: '#736c64', //upper operator button light brown
    darkbrown: '#593d1c', //right-side button operator dark brown
    btnLbrown: '#9c7952', //light(2) brown
    btnColorDark: '#cfab80',
    gray: '#5c5b5a',
    black: '#000000', //black theme
    numBG: '#FFFFFF', //number's background
    total: '#46D5B2',
}