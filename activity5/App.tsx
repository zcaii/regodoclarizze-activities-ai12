import { useState } from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Switch, SafeAreaView } from 'react-native';
import { ThemeContext } from './src/styles/DefaultTheme';
import { ThemeColors } from './src/styles/ThemeColors';
import Button from './src/components/ButtonConfig';
import ButtonDisplay from './src/components/ButtonDisplay';

export default function App() {
  const [theme, setTheme] = useState('light');
  return (
    <ThemeContext.Provider value={theme}>
    <SafeAreaView style={theme === 'light' ? styles.container : [styles.container, {backgroundColor: 'black'}]}>
      <StatusBar style="auto" />
      <Switch
        value={theme === 'light'}
        onValueChange={() => setTheme(theme === 'light' ? 'dark' : 'light')}
      />
      <ButtonDisplay/>
    </SafeAreaView>
    </ThemeContext.Provider>  
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: ThemeColors.light,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});
