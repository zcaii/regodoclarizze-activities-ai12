import React from 'react-native';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#940401',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btn:{
    height:30,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  pressButton: {
    color: "#fff",
    fontWeight: "bold",
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 25,
    marginBottom: 0
  },
  context: {
    marginTop: 10,
    fontSize: 18,
    marginLeft: 50,
    marginRight: 50,
    color: "#fff"
  },
  text: {
    top: 10,
    width: 400,
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center"
  },
  page: {
    fontWeight: "bold",
    marginTop: 10,
    fontSize: 100,
    marginLeft: 50,
    marginRight: 50,
    color: "#fff"
  },
  view: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white"
  },
  title: {
    marginTop: 10,
    marginLeft: 20
  },
  back: {
    flex: 1,
    justifyContent: "center",
  },
});


const TheAlchemist = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Image source= {require("../background/thealchemist.jpg")}
          style={{width: 400, height: 600, marginTop: 0, marginLeft: 20, marginRight: 20,}}/>
      <View style={styles.btn}>
        <TouchableOpacity onPress={() => navigation.navigate("AlchemistChapters")}>
          <View style={styles.press}>
            <Text style={styles.pressButton}>CHAPTERS</Text>
          </View>
        </TouchableOpacity>
      </View>

      <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.goBack("Books")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Books</Text>
            </View>
          </TouchableOpacity>
        </View>

      <View style={styles.btn}>
        <TouchableOpacity onPress={() => navigation.navigate("AlchemistChapters")}>
          <View style={styles.press}>
            <Text style={styles.pressButton}>Home</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default TheAlchemist;