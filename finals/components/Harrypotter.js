import { HeaderStyleInterpolators } from '@react-navigation/stack';
import React from 'react';
import { Text,
         View,
         StyleSheet } from "react-native";
import { SafeAreaView,
          ScrollView} from "react-native";
import { TouchableOpacity } from 'react-native-gesture-handler';


const styles = StyleSheet.create({
  pressButton: {
    color: '#7c0000',
    fontWeight: 'bold',
    fontSize: 30,
    marginLeft: 20,
    marginRight: 20,
  },

page: {
    fontWeight: "bold",
    marginTop: 80,
    fontSize: 50,
    marginLeft: 50,
    marginRight: 50,
    color: "#af0716"
  },
  text: {
    top: 10,
    width: 800,
    fontSize: 25,
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center"
  },
  view: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#000000",
  },
  back: {
    flex: 1,
    justifyContent: "center",
  },
  container: {
    flex: 1,
    backgroundColor: '#000000',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
  const HarryPotter = ({ navigation }) => {
    return (
        <SafeAreaView style={styles.container}>
        <Text style={styles.page}>CHAPTERS</Text>
        <ScrollView style={styles.scrollView}>
          <Text style={styles.text}>
s
      <View style={styles.container}>
        <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("B1")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Chapter 1</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("B2")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Chapter 2</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("B3")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Chapter 3</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("B4")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Chapter 4</Text>
            </View>
          </TouchableOpacity>
          </View>
          
        <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("B5")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Chapter 5</Text>
            </View>
          </TouchableOpacity>
          </View>

        <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("B6")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Chapter 6</Text>
            </View>
          </TouchableOpacity>
          </View>

          <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("B7")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Chapter 7</Text>
            </View>
          </TouchableOpacity>
          </View>

          <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("B8")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Chapter 8</Text>
            </View>
          </TouchableOpacity>
          </View>

          <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("B9")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Chapter 9</Text>
            </View>
          </TouchableOpacity>
          </View>

          <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("B10")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Chapter 10</Text>
            </View>
          </TouchableOpacity>
          </View>

          <Text>{'/n}'}</Text>

          <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("Books")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Books</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("Home Screen")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Home</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      </Text>
 
    </ScrollView>    
    </SafeAreaView>
    );
  };
  
  export default HarryPotter;