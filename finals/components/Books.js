import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Text,
         View,
         StyleSheet , 
         Image } from "react-native";
import { SafeAreaView,
          ScrollView} from "react-native";
import { TouchableOpacity } from 'react-native-gesture-handler';


const styles = StyleSheet.create({
  pressButton: {
    color: '#eed9af',
    fontWeight: 'bold',
    fontSize: 30,
    marginLeft: 50,
    marginRight: 50,
  },

page: {
    fontWeight: "bold",
    marginTop: 100,
    fontSize: 50,
    marginLeft: 50,
    marginRight: 50,
    color: "#f7eee8"
  },
  text: {
    top: 10,
    width: 800,
    fontSize: 25,
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center"
  },
  view: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#000000",
  },
  back: {
    flex: 1,
    justifyContent: "center",
  },
  container: {
    flex: 1,
    backgroundColor: '#000000',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
  
  const Books = ({ navigation }) => {
    return (
      <View style={styles.container}>
      <Image source= {require("../background/secondpage.png")}
          style={{width: 350, height: 150, marginTop: 20, marginLeft: 20, marginRight: 20,}}/>
        <SafeAreaView style={styles.container}>
        <Text style={styles.page}>Books</Text>
        <ScrollView style={styles.scrollView}>
          <Text style={styles.text}>
  
      <View style={styles.container}>
        <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("The Lord of the Ring")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>The Lord of the Rings</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("Harry Potter")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Harry Potter</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("The Alchemist")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>The Alchemist</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("The Call of the Wind")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>The Call of the Wind</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("Home Screen")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Home</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>   
</Text>
 </ScrollView>    
    </SafeAreaView>
    </View>   
   
    );
  };
  
  export default Books;