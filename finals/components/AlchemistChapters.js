import { HeaderStyleInterpolators } from '@react-navigation/stack';
import React from 'react';
import { Text,
         View,
         StyleSheet } from "react-native";
import { SafeAreaView,
          ScrollView} from "react-native";
import { TouchableOpacity } from 'react-native-gesture-handler';


const styles = StyleSheet.create({
  pressButton: {
    color: '#e87500',
    fontWeight: 'bold',
    fontSize: 30,
    marginLeft: 20,
    marginRight: 20,
  },

page: {
    fontWeight: "bold",
    marginTop: 80,
    fontSize: 50,
    marginLeft: 50,
    marginRight: 50,
    color: "#e87500"
  },
  text: {
    top: 10,
    width: 800,
    fontSize: 25,
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center"
  },
  view: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#18191a",
  },
  back: {
    flex: 1,
    justifyContent: "center",
  },
  container: {
    flex: 1,
    backgroundColor: '#18191a',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
  
  const AlchemistChapters = ({ navigation }) => {
    return (
        <SafeAreaView style={styles.container}>
        <Text style={styles.page}>CHAPTERS</Text>
        <ScrollView style={styles.scrollView}>
          <Text style={styles.text}>

      <View style={styles.container}>
        <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("D1")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Prologue</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("D2")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Chapter 1</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("D3")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Chapter 2</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("D4")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Epilogue</Text>
            </View>
          </TouchableOpacity>
          </View>

          <Text></Text>

          <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("Books")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Books</Text>
            </View>
          </TouchableOpacity>
        </View>
        
        <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.navigate("Home Screen")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Home</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      </Text>
 
    </ScrollView>    
    </SafeAreaView>
    );
  };
  
  export default AlchemistChapters;