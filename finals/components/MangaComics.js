import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import {WebView} from 'react-native-webview';

const styles = StyleSheet.create({
    btn: {
        paddingTop: 10,
        marginLeft: 5
      },
        pressButton: {
          color: "#000000",
          fontWeight: "bold",
          alignItems: 'center',
          justifyContent: 'center',
          fontSize: 20,
          marginLeft: 10,
          marginBottom: 0
          },
      text: {
        textAlign: 'justify',
        marginTop: 10,
        fontSize: 50,
        marginLeft: 10,
        marginRight: 10,
        color: "#000000"
      },
});

const MangaComics = ({ navigation }) => {
  return (
    <View style={{flex: 1}}>
      <WebView source={{uri: 'https://manganato.com/'}} />
      <View style={styles.btn}>
        <TouchableOpacity onPress={() => navigation.push("MangaComics")}>
          <View style={styles.press}>
            <Text style={styles.pressButton}>Back</Text>
          </View>
        </TouchableOpacity>
      </View>

      <View style={styles.btn}>
        <TouchableOpacity onPress={() => navigation.popToTop()}>
          <View style={styles.press}>
            <Text style={styles.pressButton}>Home</Text>
          </View>
        </TouchableOpacity>
      </View>

    </View>
    
  );
};

export default MangaComics;