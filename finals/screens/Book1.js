import React from 'react-native';
import { Text, View, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

const styles = StyleSheet.create({
  btn: {
    paddingTop: 10,
    marginLeft: 5
  },
    pressButton: {
      color: "#000000",
      fontWeight: "bold",
      alignItems: 'center',
      justifyContent: 'center',
      fontSize: 20,
      marginLeft: 10,
      marginBottom: 0
      },
  text: {
    textAlign: 'justify',
    marginTop: 10,
    fontSize: 18,
    marginLeft: 10,
    marginRight: 10,
    color: "#000000"
  },
  page: {
    fontWeight: "bold",
    marginTop: 10,
    fontSize: 25,
    marginLeft: 10,
    marginRight: 10,
    color: "#000000",
  },
  context: {
    top: 10,
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center",
    color: "#857171"
  },
  view: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
  },
  title: {
  marginTop: 1,
  marginLeft: 10
  },
  back: {
    flex: 1,
    justifyContent: "center",
  },
});


const Book1 = ({ navigation }) => {
    return (
      <SafeAreaView style={styles.container}>
      <Text style={styles.page}>Chapter 1</Text>
      <Text style={styles.page}>The Shadow of the Past</Text>

      <View style={styles.btn}>
        <TouchableOpacity onPress={() => navigation.navigate("B2")}>
          <View style={styles.press}>
            <Text style={styles.pressButton}>Chapter 2</Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.btn}>
        <TouchableOpacity onPress={() => navigation.navigate("Chapters")}>
          <View style={styles.press}>
            <Text style={styles.pressButton}>Go back</Text>
          </View>
        </TouchableOpacity>
      </View>

      <ScrollView style={styles.scrollView}>
        <Text style={styles.text}>
            M r. and Mrs. Dursley, of number four, Privet Drive, were proud to say
that they were perfectly normal, thank you very much. They were the last people
youd expect to be involved in anything strange or mysterious, because they just
didnt hold with such nonsense.
{'\n'}{'\n'}
Mr. Dursley was the director of a firm called Grunnings, which made
drills. He was a big, beefy man with hardly any neck, although he did have a
very large mustache. Mrs. Dursley was thin and blonde and had nearly twice the
usual amount of neck, which came in very useful as she spent so much of her
time craning over garden fences, spying on the neighbors. The Dursleys had a
small son called Dudley and in their opinion there was no finer boy anywhere.
The Dursleys had everything they wanted, but they also had a secret, and
their greatest fear was that somebody would discover it. They didn't think they
could bear it if anyone found out about the Potters. Mrs. Potter was Mrs.
Dursleys sister, but they hadnt met for several years; in fact, Mrs. Dursley
pretended she didnt have a sister, because her sister and her good-for-nothing
husband were as unDursleyish as it was possible to be. The Dursleys shuddered
to think what the neighbors would say if the Potters arrived in the street. The
Dursleys knew that the Potters had a small son, too, but they had never even
seen him. This boy was another good reason for keeping the Potters away; they
didnt want Dudley mixing with a child like that.
{'\n'}{'\n'}
When Mr. and Mrs. Dursley woke up on the dull, gray Tuesday our story
starts, there was nothing about the cloudy sky outside to suggest that strange and
mysterious things would soon be happening all over the country. Mr. Dursley
hummed as he picked out his most boring tie for work, and Mrs. Dursley
gossiped away happily as she wrestled a screaming Dudley into his high chair.
None of them noticed a large, tawny owl flutter past the window.{'\n'}{'\n'}
At half past eight, Mr. Dursley picked up his briefcase, pecked Mrs.{'\n'}{'\n'}
Dursley on the cheek, and tried to kiss Dudley good-bye but missed, because
Dudley was now having a tantrum and throwing his cereal at the walls.
“Little tyke,” chortled Mr. Dursley as he left the house. He got into his car
and backed out of number fours drive.{'\n'}{'\n'}
It was on the corner of the street that he noticed the first sign of
something peculiar — a cat reading a map. For a second, Mr. Dursley didnt
realize what he had seen — then he jerked his head around to look again. There
was a tabby cat standing on the corner of Privet Drive, but there wasnt a map in
sight. What could he have been thinking of? It must have been a trick of the
light. Mr. Dursley blinked and stared at the cat. It stared back. As Mr. Dursley
drove around the corner and up the road, he watched the cat in his mirror. It was
now reading the sign that said Privet Drive — no, looking at the sign; cats
couldnt read maps or signs. Mr. Dursley gave himself a little shake and put the
cat out of his mind. As he drove toward town he thought of nothing except a
large order of drills he was hoping to get that day.{'\n'}{'\n'}
But on the edge of town, drills were driven out of his mind by something
else. As he sat in the usual morning traffic jam, he couldnt help noticing that
there seemed to be a lot of strangely dressed people about. People in cloaks. Mr.
Dursley couldnt bear people who dressed in funny clothes — the getups you
saw on young people! He supposed this was some stupid new fashion. He
drummed his fingers on the steering wheel and his eyes fell on a huddle of these
weirdos standing quite close by. They were whispering excitedly together. Mr.
Dursley was enraged to see that a couple of them werent young at all; why, that
man had to be older than he was, and wearing an emerald-green cloak! The
nerve of him! But then it struck Mr. Dursley that this was probably some silly
stunt —these people were obviously collecting for something…yes, that would
be it. The traffic moved on and a few minutes later, Mr. Dursley arrived in the
Grunnings parking lot, his mind back on drills.{'\n'}{'\n'}
Mr. Dursley always sat with his back to the window in his office on the
ninth floor. If he hadnt, he might have found it harder to concentrate on drills
that morning. He didnt see the owls swooping past in broad daylight, though
people down in the street did; they pointed and gazed open-mouthed as owl after
owl sped overhead. Most of them had never seen an owl even at nighttime. Mr.
Dursley, however, had a perfectly normal, owl-free morning. He yelled at five
different people. He made several important telephone calls and shouted a bit
more. He was in a very good mood until lunchtime, when he thought hed stretch
his legs and walk across the road to buy himself a bun from the bakery.
Hed for gotten all about the people in cloaks until he passed a group of
them next to the bakers. He eyed them angrily as he passed. He didnt know
why, but they made him uneasy. This bunch were whispering excitedly, too, and
he couldnt see a single collecting tin. It was on his way back past them,
clutching a large doughnut in a bag, that he caught a few words of what they
were saying.{'\n'}{'\n'}
“The Potters, thats right, thats what I heard —”{'\n'}{'\n'}
“ — yes, their son, Harry —”{'\n'}{'\n'}
Mr. Dursley stopped dead. Fear flooded him. He looked back at the
whisperers as if he wanted to say something to them, but thought better of it.
He dashed back across the road, hurried up to his office, snapped at his
secretary not to disturb him, seized his telephone, and had almost finished
dialing his home number when he changed his mind. He put the receiver back
down and stroked his mustache, thinking…no, he was being stupid. Potter
wasnt such an unusual name. He was sure there were lots of people called Potter
who had a son called Harry. Come to think of it, he wasnt even sure his nephew
was called Harry. Hed never even seen the boy. It might have been Harvey. Or
Harold. There was no point in worrying Mrs. Dursley; she always got so upset at
any mention of her sister. He didnt blame her — if hed had a sister like that…
but all the same, those people in cloaks.…{'\n'}{'\n'}
He found it a lot harder to concentrate on drills that afternoon and when
he left the building at five oclock, he was still so worried that he walked straight
into someone just outside the door.{'\n'}{'\n'}
“Sorry,” he grunted, as the tiny old man stumbled and almost fell. It was
a few seconds before Mr. Dursley realized that the man was wearing a violet
cloak. He didnt seem at all upset at being almost knocked to the ground. On the
contrary, his face split into a wide smile and he said in a squeaky voice that made
passersby stare, “Dont be sorry, my dear sir, for nothing could upset me today!
Rejoice, for You-Know-Who has gone at last! Even Muggles like yourself
should be celebrating, this happy, happy day!”{'\n'}{'\n'}
And the old man hugged Mr. Dursley around the middle and walked off.
Mr. Dursley stood rooted to the spot. He had been hugged by a complete
stranger. He also thought he had been called a Muggle, whatever that was. He
was rattled. He hurried to his car and set off for home, hoping he was imagining
things, which he had never hoped before, because he didnt approve of
imagination.{'\n'}{'\n'}
As he pulled into the driveway of number four, the first thing he saw —
and it didnt improve his mood — was the tabby cat hed spotted that morning. It
was now sitting on his garden wall. He was sure it was the same one; it had the
same markings around its eyes.{'\n'}{'\n'}
“Shoo!” said Mr. Dursley loudly.{'\n'}{'\n'}
The cat didnt move. It just gave him a stern look. Was this normal cat
behavior? Mr. Dursley wondered. Trying to pull himself together, he let himself
into the house. He was still determined not to mention anything to his wife.
Mrs. Dursley had had a nice, normal day. She told him over dinner all
about Mrs. Next Doors problems with her daughter and how Dudley had
learned a new word (“Wont!”). Mr. Dursley tried to act normally. When Dudley
had been put to bed, he went into the living room in time to catch the last report
on the evening news:{'\n'}{'\n'}
“And finally, bird-watchers everywhere have reported that the nations
owls have been behaving very unusually today. Although owls normally hunt at
night and are hardly ever seen in daylight, there have been hundreds of sightings
of these birds flying in every direction since sunrise. Experts are unable to
explain why the owls have suddenly changed their sleeping pattern.” The
newscaster allowed himself a grin. “Most mysterious. And now, over to Jim
McGuffin with the weather. Going to be any more showers of owls tonight,
Jim?”{'\n'}{'\n'}
“Well, Ted,” said the weatherman, “I dont know about that, but its not
only the owls that have been acting oddly today. Viewers as far apart as Kent,
Yorkshire, and Dundee have been phoning in to tell me that instead of the rain I
promised yesterday, theyve had a downpour of shooting stars! Perhaps people
have been celebrating Bonfire Night early — its not until next week, folks! But
I can promise a wet night tonight.”{'\n'}{'\n'}
Mr. Dursley sat frozen in his armchair. Shooting stars all over Britain?
Owls flying by daylight? Mysterious people in cloaks all over the place? And a
whisper, a whisper about the Potters.…{'\n'}{'\n'}
Mrs. Dursley came into the living room carrying two cups of tea. It was
no good. Hed have to say something to her. He cleared his throat nervously. “Er
— Petunia, dear — you havent heard from your sister lately, have you?”
As he had expected, Mrs. Dursley looked shocked and angry. After all,
they normally pretended she didnt have a sister.
“No,” she said sharply. “Why?”{'\n'}{'\n'}
“Funny stuff on the news,” Mr. Dursley mumbled. “Owls…shooting
stars…and there were a lot of funny-looking people in town today.…”{'\n'}{'\n'}
“So?” snapped Mrs. Dursley.{'\n'}{'\n'}
“Well, I just thought…maybe…it was something to do with…you
know…her crowd.”{'\n'}{'\n'}
Mrs. Dursley sipped her tea through pursed lips. Mr. Dursley wondered
whether he dared tell her hed heard the name “Potter.” He decided he didnt
dare. Instead he said, as casually as he could, “Their son — hed be about
Dudleys age now, wouldnt he?”{'\n'}{'\n'}
“I suppose so,” said Mrs. Dursley stiffly.
“Whats his name again? Howard, isnt it?”{'\n'}{'\n'}
“Harry. Nasty, common name, if you ask me.”{'\n'}{'\n'}
“Oh, yes,” said Mr. Dursley, his heart sinking horribly. “Yes, I quite
agree.”{'\n'}{'\n'}
He didnt say another word on the subject as they went upstairs to bed.{'\n'}{'\n'}
While Mrs. Dursley was in the bathroom, Mr. Dursley crept to the bedroom
window and peered down into the front garden. The cat was still there. It was
staring down Privet Drive as though it were waiting for something.{'\n'}{'\n'}
Was he imagining things? Could all this have anything to do with the
Potters? If it did...if it got out that they were related to a pair of — well, he didnt
think he could bear it.{'\n'}{'\n'}
The Dursleys got into bed. Mrs. Dursley fell asleep quickly but Mr.
Dursley lay awake, turning it all over in his mind. His last, comforting thought
before he fell asleep was that even if the Potters were involved, there was no
reason for them to come near him and Mrs. Dursley. The Potters knew very well
what he and Petunia thought about them and their kind....He couldnt see how he
and Petunia could get mixed up in anything that might be going on — he
yawned and turned over — it couldnt affect them.…{'\n'}{'\n'}
How very wrong he was.{'\n'}{'\n'}
Mr. Dursley might have been drifting into an uneasy sleep, but the cat on
the wall outside was showing no sign of sleepiness. It was sitting as still as a
statue, its eyes fixed unblinkingly on the far corner of Privet Drive. It didnt so
much as quiver when a car door slammed on the next street, nor when two owls
swooped overhead. In fact, it was nearly midnight before the cat moved at all.
A man appeared on the corner the cat had been watching, appeared so
suddenly and silently youd have thought hed just popped out of the ground.
The cats tail twitched and its eyes narrowed.{'\n'}{'\n'}
Nothing like this man had ever been seen on Privet Drive. He was tall,
thin, and very old, judging by the silver of his hair and beard, which were both
long enough to tuck into his belt. He was wearing long robes, a purple cloak that
swept the ground, and high-heeled, buckled boots. His blue eyes were light,
bright, and sparkling behind half-moon spectacles and his nose was very long
and crooked, as though it had been broken at least twice. This mans name was
Albus Dumbledore.{'\n'}{'\n'}
Albus Dumbledore didnt seem to realize that he had just arrived in a
street where everything from his name to his boots was unwelcome. He was
busy rummaging in his cloak, looking for something. But he did seem to realize
he was being watched, because he looked up suddenly at the cat, which was still
staring at him from the other end of the street. For some reason, the sight of the
cat seemed to amuse him. He chuckled and muttered, “I should have known.”{'\n'}{'\n'}
He found what he was looking for in his inside pocket. It seemed to be a
silver cigarette lighter. He flicked it open, held it up in the air, and clicked it. The
nearest street lamp went out with a little pop. He clicked it again — the next
lamp flickered into darkness. Twelve times he clicked the Put-Outer, until the
only lights left on the whole street were two tiny pinpricks in the distance, which
were the eyes of the cat watching him. If anyone looked out of their window
now, even beady-eyed Mrs. Dursley, they wouldnt be able to see anything that
was happening down on the pavement. Dumbledore slipped the Put-Outer back
inside his cloak and set off down the street toward number four, where he sat
down on the wall next to the cat. He didnt look at it, but after a moment he
spoke to it.{'\n'}{'\n'}
“Fancy seeing you here, Professor McGonagall.”
He turned to smile at the tabby, but it had gone. Instead he was smiling at
a rather severe-looking woman who was wearing square glasses exactly the
shape of the markings the cat had had around its eyes. She, too, was wearing a
cloak, an emerald one. Her black hair was drawn into a tight bun. She looked
distinctly ruffled.{'\n'}{'\n'}
“How did you know it was me?” she asked.{'\n'}{'\n'}
“My dear Professor, Ive never seen a cat sit so stiffly.”{'\n'}{'\n'}
“Youd be stiff if youd been sitting on a brick wall all day,” said{'\n'}{'\n'}
Professor McGonagall.{'\n'}{'\n'}
“All day? When you could have been celebrating? I must have passed a
dozen feasts and parties on my way here.”{'\n'}{'\n'}
Professor McGonagall sniffed angrily.{'\n'}{'\n'}
“Oh yes, Ive celebrating, all right,” she said impatiently. “Youd think
theyd be a bit more careful, but no —even the Muggles have noticed
somethings going on. It was on their news.” She jerked her head back at the
Dursleys dark living-room window. “I heard it. Flocks of owls…shooting
stars…Well, theyre not completely stupid. They were bound to notice
something. Shooting stars down in Kent — Ill bet that was Dedalus Diggle. He
never had much sense.”{'\n'}{'\n'}
“You cant blame them,” said Dumbledore gently. “Weve had precious
little to celebrate for eleven years.”{'\n'}{'\n'}
“I know that,” said Professor McGonagall irritably. “But thats no reason
to lose our heads. People are being downright careless, out on the streets in
broad daylight, not even dressed in Muggle clothes, swapping rumors.”{'\n'}{'\n'}
She threw a sharp, sideways glance at Dumbledore here, as though
hoping he was going to tell her something, but he didnt, so she went on. “A fine
thing it would be if, on the very day You-Know-Who seems to have disappeared
at last, the Muggles found out about us all. I suppose he really has gone,
Dumbledore?”{'\n'}{'\n'}
“It certainly seems so,” said Dumbledore. “We have much to be thankful
for. Would you care for a lemon drop?”{'\n'}{'\n'}
“A what?”{'\n'}{'\n'}
“A lemon drop. Theyre a kind of Muggle sweet Im rather fond of.”{'\n'}{'\n'}
“No, thank you,” said Professor McGonagall coldly, as though she didnt
think this was the moment for lemon drops. “As I say, even if You-Know-Who
has gone —”{'\n'}{'\n'}
“My dear Professor, surely a sensible person like yourself can call him by
his name? All this You-Know-Who nonsense — for eleven years I have been
trying to persuade people to call him by his proper name: Voldemort.” Professor
McGonagall flinched, but Dumbledore, who was unsticking two lemon drops,
seemed not to notice. “It all gets so confusing if we keep saying You-KnowWho. I have never seen any reason to be frightened of saying Voldemorts
name.”{'\n'}{'\n'}
“I know you havent, said Professor McGonagall, sounding half
exasperated, half admiring. “But youre different. Everyone knows youre the
only one You-Know- oh, all right, Voldemort, was frightened of.”{'\n'}{'\n'}
“You flatter me,” said Dumbledore calmly. “Voldemort had powers I will
never have.”{'\n'}{'\n'}
“Only because you're too — well — noble to use them.”{'\n'}{'\n'}
“Its lucky its dark. I havent blushed so much since Madam Pomfrey
told me she liked my new earmuffs.”{'\n'}{'\n'}
Professor McGonagall shot a sharp look at Dumbledore and said “The
owls are nothing next to the rumors that are flying around. You know what
theyre saying? About why hes disappeared? About what finally stopped him?”
It seemed that Professor McGonagall had reached the point she was most
anxious to discuss, the real reason she had been waiting on a cold, hard wall all
day, for neither as a cat nor as a woman had she fixed Dumbledore with such a
piercing stare as she did now. It was plain that whatever “everyone” was saying,
she was not going to believe it until Dumbledore told her it was true.{'\n'}{'\n'}
Dumbledore, however, was choosing another lemon drop and did not answer.{'\n'}{'\n'}
“What theyre saying,” she pressed on, “is that last night Voldemort{'\n'}{'\n'}
turned up in Godrics Hollow. He went to find the Potters. The rumor is that Lily
and James Potter are — are — that theyre — dead.”{'\n'}{'\n'}
Dumbledore bowed his head. Professor McGonagall gasped.{'\n'}{'\n'}
“Lily and James…I cant believe it…I didnt want to believe it…Oh,
Albus…”{'\n'}{'\n'}
Dumbledore reached out and patted her on the shoulder. “I know…I
know…” he said heavily.{'\n'}{'\n'}
Professor McGonagalls voice trembled as she went on. “Thats not all.
Theyre saying he tried to kill the Potters son, Harry. But he couldnt. He
couldnt kill that little boy. No one knows why, or how, but theyre saying that
when he couldnt kill Harry Potter, Voldemorts power somehow broke — and
thats why hes gone.”{'\n'}{'\n'}
Dumbledore nodded glumly.{'\n'}{'\n'}
“Its — its true?” faltered Professor McGonagall. “After all hes done…
all the people hes killed…he couldnt kill a little boy? Its just astounding…of
all the things to stop him…but how in the name of heaven did Harry survive?”
“We can only guess.” said Dumbledore. “We may never know.”{'\n'}{'\n'}
Professor McGonagall pulled out a lace handkerchief and dabbed at her
eyes beneath her spectacles. Dumbledore gave a great sniff as he took a golden
watch from his pocket and examined it. It was a very odd watch. It had twelve
hands but no numbers; instead, little planets were moving around the edge. It
must have made sense to Dumbledore, though, because he put it back in his
pocket and said, “Hagrids late. I suppose it was he who told you Id be here, by
the way?”{'\n'}{'\n'}
“Yes,” said Professor McGonagall. “And I dont suppose youre going to
tell me why youre here, of all places?”{'\n'}{'\n'}
“Ive come to bring Harry to his aunt and uncle. Theyre the only family
he has left now.”{'\n'}{'\n'}
“You dont mean  you cant mean the people who live here?” cried
Professor McGonagall, jumping to her feet and pointing at number four.
“Dumbledore — you cant. Ive been watching them all day. You couldnt find
two people who are less like us. And they've got this son — I saw him kicking
his mother all the way up the street, screaming for sweets. Harry Potter come
and live here!”{'\n'}{'\n'}
“Its the best place for him,” said Dumbledore firmly. “His aunt and
uncle will be able to explain everything to him when hes older. Ive written
them a letter.”{'\n'}{'\n'}
“A letter?” repeated Professor McGonagall faintly, sitting back down on
the wall. “Really, Dumbledore, you think you can explain all this in a letter?
These people will never understand him! Hell be famous — a legend — I
wouldnt be surprised if today was known as Harry Potter day in the future —
there will be books written about Harry — every child in our world will know
his name!”{'\n'}{'\n'}
“Exactly.” said Dumbledore, looking very seriously over the top of his
half-moon glasses. “It would be enough to turn any boys head. Famous before
he can walk and talk! Famous for something he wont even remember! Can you
see how much better off hell be, growing up away from all that until hes ready
to take it?”{'\n'}{'\n'}
Professor McGonagall opened her mouth, changed her mind, swallowed,
and then said, “Yes — yes, youre right, of course. But how is the boy getting
here, Dumbledore?” She eyed his cloak suddenly as though she thought he might
be hiding Harry underneath it.{'\n'}{'\n'}
“Hagrids bringing him.”{'\n'}{'\n'}
“You think it — wise — to trust Hagrid with something as important as
this?”{'\n'}{'\n'}
“I would trust Hagrid with my life,” said Dumbledore.
“Im not saying his heart isn't in the right place,” said Professor
McGonagall grudgingly, “but you can't pretend he's not careless. He does tend
to — what was that?”{'\n'}{'\n'}
A low rumbling sound had broken the silence around them. It grew
steadily louder as they looked up and down the street for some sign of a
headlight; it swelled to a roar as they both looked up at the sky — and a huge
motorcycle fell out of the air and landed on the road in front of them.
If the motorcycle was huge, it was nothing to the man sitting astride it.
He was almost twice as tall as a normal man and at least five times as wide. He
looked simply too big to be allowed, and so wild — long tangles of bushy black
hair and beard hid most of his face, he had hands the size of trash can lids, and
his feet in their leather boots were like baby dolphins. In his vast, muscular arms
he was holding a bundle of blankets.{'\n'}{'\n'}
“Hagrid,” said Dumbledore, sounding relieved. “At last. And where did
you get that motorcycle?”{'\n'}{'\n'}
“Borrowed it, Professor Dumbledore, sir,” said the giant, climbing
carefully off the motorcycle as he spoke. “Young Sirius Black lent it to me. Ive
got him, sir.”{'\n'}{'\n'}
“No problems, were there?”{'\n'}{'\n'}
“No, sir — house was almost destroyed, but I got him out all right before
the Muggles started swarmin' around. He fell asleep as we was flyin' over
Bristol.”{'\n'}{'\n'}
Dumbledore and Professor McGonagall bent forward over the bundle of
blankets. Inside, just visible, was a baby boy, fast asleep. Under a tuft of jetblack hair over his forehead they could see a curiously shaped cut, like a bolt of
lightning.{'\n'}{'\n'}
“Is that where —?” whispered Professor McGonagall.{'\n'}{'\n'}
“Yes,” said Dumbledore. “He" ll have that scar forever.”{'\n'}{'\n'}
“Couldn't you do something about it, Dumbledore?”{'\n'}{'\n'}
“Even if I could, I wouldn't. Scars can come in handy. I have one myself
above my left knee that is a perfect map of the London Underground. Well —
give him here, Hagrid — we'd better get this over with.”
Dumbledore took Harry in his arms and turned toward the Dursleys'
house.{'\n'}{'\n'}
“Could I — could I say good-bye to him, sir?” asked Hagrid. He bent his
great, shaggy head over Harry and gave him what must have been a very
scratchy, whiskery kiss. Then, suddenly, Hagrid let out a howl like a wounded
dog.{'\n'}{'\n'}
“Shhh!” hissed Professor McGonagall, “You'll wake the Muggles!”{'\n'}{'\n'}
“S-s-sorry,” sobbed Hagrid, taking out a large, spotted handkerchief and
burying his face in it. “But I c-c-can't stand it —Lily an'James dead — an' poor
little Harry off ter live with Muggles —”{'\n'}{'\n'}
“Yes, yes, it's all very sad, but get a grip on yourself, Hagrid, or we'll be
found,” Professor McGonagall whispered, patting Hagrid gingerly on the arm as
Dumbledore stepped over the low garden wall and walked to the front door. He
laid Harry gently on the doorstep, took a letter out of his cloak, tucked it inside
Harry's blankets, and then came back to the other two. For a full minute the
three of them stood and looked at the little bundle; Hagrid's shoulders shook,
Professor McGonagall blinked furiously, and the twinkling light that usually
shone from Dumbledore's eyes seemed to have gone out.
“Well,” said Dumbledore finally, “that's that. We've no business staying
here. We may as well go and join the celebrations.”{'\n'}{'\n'}
“Yeah,” said Hagrid in a very muffled voice, “I'll be takin' Sirius his bike
back. G'night, Professor McGonagall — Professor Dumbledore, sir.”{'\n'}{'\n'}
Wiping his streaming eyes on his jacket sleeve, Hagrid swung himself
onto the motorcycle and kicked the engine into life; with a roar it rose into the
air and off into the night.{'\n'}{'\n'}
“I shall see you soon, I expect, Professor McGonagall,” said{'\n'}{'\n'}
Dumbledore, nodding to her. Professor McGonagall blew her nose in reply.
Dumbledore turned and walked back down the street. On the corner he
stopped and took out the silver Put-Outer. He clicked it once, and twelve balls of
light sped back to their street lamps so that Privet Drive glowed suddenly orange
and he could make out a tabby cat slinking around the corner at the other end of
the street. He could just see the bundle of blankets on the step of number four.
“Good luck, Harry,” he murmured. He turned on his heel and with a
swish of his cloak, he was gone.{'\n'}{'\n'}
A breeze ruffled the neat hedges of Privet Drive, which lay silent and tidy
under the inky sky, the very last place you would expect astonishing things to
happen. Harry Potter rolled over inside his blankets without waking up. One
small hand closed on the letter beside him and he slept on, not knowing he was
special, not knowing he was famous, not knowing he would be woken in a few
hours' time by Mrs. Dursley's scream as she opened the front door to put out the
milk bottles, nor that he would spend the next few weeks being prodded and
pinched by his cousin Dudley....He couldn't know that at this very moment,
people meeting in secret all over the country were holding up their glasses and
saying in hushed voices: “To Harry Potter — the boy who lived!”{'\n'}{'\n'}

</Text>

    </ScrollView>
    </SafeAreaView>
);
};

  export default Book1;