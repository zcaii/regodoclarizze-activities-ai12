import React from 'react-native';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

const styles = StyleSheet.create({
  btn: {
    paddingTop: 10,
    marginLeft: 5
  },
    pressButton: {
      color: "#000000",
      fontWeight: "bold",
      alignItems: 'center',
      justifyContent: 'center',
      fontSize: 20,
      marginLeft: 10,
      marginBottom: 0
      },
  text: {
    textAlign: 'justify',
    marginTop: 10,
    fontSize: 18,
    marginLeft: 10,
    marginRight: 10,
    color: "#000000"
  },
  page: {
    fontWeight: "bold",
    marginTop: 10,
    fontSize: 25,
    marginLeft: 10,
    marginRight: 10,
    color: "#000000",
  },
  context: {
    top: 10,
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center",
    color: "#857171"
  },
  view: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
  },
  title: {
  marginTop: 1,
  marginLeft: 10
  },
  back: {
    flex: 1,
    justifyContent: "center",
  },
});

const Chapter7 = ({ navigation }) => {
    return (
      <SafeAreaView style={styles.container}>
        <Text style={styles.page}>Chapter 7</Text>
        <Text style={styles.page}>In the House of Tom Bombadil</Text>
        <View style={styles.btn}>
        <TouchableOpacity onPress={() => navigation.navigate("C8")}>
          <View style={styles.press}>
            <Text style={styles.pressButton}>Chapter 8</Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.btn}>
        <TouchableOpacity onPress={() => navigation.navigate("CHAPTERS")}>
          <View style={styles.press}>
            <Text style={styles.pressButton}>Go back</Text>
          </View>
        </TouchableOpacity>
      </View>
        <ScrollView style={styles.scrollView}>
          <Text style={styles.text}>
          The four hobbits stepped over the wide stone threshold, and stood still, blinking.
They were in a long low room, filled with the light of lamps swinging from the
beams of the roof; and on the table of dark polished wood stood many candles,
tall and yellow, burning brightly. In a chair, at the far side of the room facing the
outer door, sat a woman. Her long yellow hair rippled down her shoulders; her
gown was green, green as young reeds, shot with silver like beads of dew; and
her belt was of gold, shaped like a chain of flag-lilies set with the pale-blue eyes
of forget-me-nots. About her feel in wide vessels of green and brown
earthenware, white water-lilies were floating, so that she seemed to be enthroned
in the midst of a pool.
{'\n'}{'\n'}
'Enter, good guests!' she said, and as she spoke they knew that it was her clear
voice they had heard singing. They came a few timid steps further into the room,
and began to bow low, feeling strangely surprised and awkward, like folk that,
knocking at a cottage door to beg for a drink of water, have been answered by a
fair young elf-queen clad in living flowers. But before they could say anything,
she sprang lightly up and over the lily-bowls, and ran laughing towards them;
and as she ran her gown rustled softly like the wind in the flowering borders of a
river.
{'\n'}{'\n'}
'Come dear folk!' she said, taking Frodo by the hand. 'Laugh and be merry! I am
{'\n'}{'\n'}
Goldberry, daughter of the River.' Then lightly she passed them and closing the
door she turned her back to it, with her white arms spread out across it. 'Let us
shut out the night!' she said. 'For you are still afraid, perhaps, of mist and treeshadows and deep water, and untame things. Fear nothing! For tonight you are
under the roof of Tom Bombadil.'
{'\n'}{'\n'}
The hobbits looked at her in wonder; and she looked at each of them and smiled.
{'\n'}{'\n'}
'Fair lady Goldberry!' said Frodo at last, feeling his heart moved with a joy that
he did not understand. He stood as he had at times stood enchanted by fair elvenvoices; but the spell that was now laid upon him was different: less keen and
lofty was the delight, but deeper and nearer to mortal heart; marvellous and yet
not strange. 'Fair lady Goldberry!' he said again. 'Now the joy that was hidden in
the songs we heard is made plain to me. O slender as a willow-wand! O clearer

</Text>
    </ScrollView>
    </SafeAreaView>
    );
  };

  export default Chapter7;