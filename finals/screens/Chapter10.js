import React from 'react-native';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

const styles = StyleSheet.create({
  btn: {
    paddingTop: 10,
    marginLeft: 5
  },
    pressButton: {
      color: "#000000",
      fontWeight: "bold",
      alignItems: 'center',
      justifyContent: 'center',
      fontSize: 20,
      marginLeft: 10,
      marginBottom: 0
      },
  text: {
    textAlign: 'justify',
    marginTop: 10,
    fontSize: 18,
    marginLeft: 10,
    marginRight: 10,
    color: "#000000"
  },
  page: {
    fontWeight: "bold",
    marginTop: 10,
    fontSize: 25,
    marginLeft: 10,
    marginRight: 10,
    color: "#000000",
  },
  context: {
    top: 10,
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center",
    color: "#857171"
  },
  view: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
  },
  title: {
  marginTop: 1,
  marginLeft: 10
  },
  back: {
    flex: 1,
    justifyContent: "center",
  },
});

const Chapter10 = ({ navigation }) => {
    return (
      <SafeAreaView style={styles.container}>
        <Text style={styles.page}>Chapter 10</Text>
        <Text style={styles.page}>Strider</Text>
        <View style={styles.btn}>
        <TouchableOpacity onPress={() => navigation.navigate("C11")}>
          <View style={styles.press}>
            <Text style={styles.pressButton}>Chapter 11</Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.btn}>
        <TouchableOpacity onPress={() => navigation.navigate("CHAPTERS")}>
          <View style={styles.press}>
            <Text style={styles.pressButton}>Go back</Text>
          </View>
        </TouchableOpacity>
      </View>
        <ScrollView style={styles.scrollView}>
          <Text style={styles.text}>
 Strider{'\n'}{'\n'}
Frodo, Pippin, and Sam made their way back to the parlour. There was no light.
Merry was not there, and the fire had burned low. It was not until they had
puffed up the embers into a blaze and thrown on a couple of faggots that they
discovered Strider had come with them. There he was calmly sitting in a chair by
the door!
{'\n'}{'\n'}
'Hallo!' said Pippin. 'Who are you, and what do you want?'
{'\n'}{'\n'}
'I am called Strider,' he answered: 'and though he may have forgotten it, your
friend promised to have a quiet talk with me.'
{'\n'}{'\n'}
'You said I might hear something to my advantage, I believe,' said Frodo.
'What have you to say?'
{'\n'}{'\n'}
'Several things,' answered Strider. 'But, of course, I have my price.'
'What do you mean?' asked Frodo sharply.
{'\n'}{'\n'}
'Don't be alarmed! I mean just this: I will tell you what I know, and give you
some good advice – but I shall want a reward.'
{'\n'}{'\n'}
'And what will that be, pray?' said Frodo. He suspected now that he had fallen in
with a rascal, and he thought uncomfortably that he had brought only a little
money with him. All of it would hardly satisfy a rogue, and he could not spare
any of it.
{'\n'}{'\n'}
'No more than you can afford,' answered Strider with a slow smile, as if he
guessed Frodo's thoughts. 'Just this: you must take me along with you, until I
wish to leave you.'
{'\n'}{'\n'}
'Oh, indeed!' replied Frodo, surprised, but not much relieved. 'Even if I wanted
another companion, I should not agree to any such thing, until I knew a good
deal more about you, and your business.'
{'\n'}{'\n'}
'Excellent!' exclaimed Strider, crossing his legs and sitting back comfortably.
{'\n'}
{'\n'}
</Text>

      
    </ScrollView>
    </SafeAreaView>
    );
  };

  export default Chapter10;