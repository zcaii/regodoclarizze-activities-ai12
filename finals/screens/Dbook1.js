import React from 'react-native';
import { Text, View, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

const styles = StyleSheet.create({
  btn: {
    paddingTop: 10,
    marginLeft: 5
  },
    pressButton: {
      color: "#000000",
      fontWeight: "bold",
      alignItems: 'center',
      justifyContent: 'center',
      fontSize: 20,
      marginLeft: 10,
      marginBottom: 0
      },
  text: {
    textAlign: 'justify',
    marginTop: 10,
    fontSize: 18,
    marginLeft: 10,
    marginRight: 10,
    color: "#000000"
  },
  page: {
    fontWeight: "bold",
    marginTop: 10,
    fontSize: 25,
    marginLeft: 10,
    marginRight: 10,
    color: "#000000",
  },
  context: {
    top: 10,
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center",
    color: "#857171"
  },
  view: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
  },
  title: {
  marginTop: 1,
  marginLeft: 10
  },
  back: {
    flex: 1,
    justifyContent: "center",
  },
});


const Dbook1 = ({ navigation }) => {
    return (
      <SafeAreaView style={styles.container}>
      <Text style={styles.page}>Prologue</Text>

      <View style={styles.btn}>
        <TouchableOpacity onPress={() => navigation.navigate("D2")}>
          <View style={styles.press}>
            <Text style={styles.pressButton}>Chapter 1</Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.btn}>
        <TouchableOpacity onPress={() => navigation.navigate("AlchemistChapters")}>
          <View style={styles.press}>
            <Text style={styles.pressButton}>Go back</Text>
          </View>
        </TouchableOpacity>
      </View>

      <ScrollView style={styles.scrollView}>
        <Text style={styles.text}>
        THE ALCHEMIST PICKED UP A BOOK THAT SOMEONE IN THE caravan had
brought. Leafing through the pages, he found a story about
Narcissus.
{'\n'}{'\n'}
The alchemist knew the legend of Narcissus, a youth who knelt
daily beside a lake to contemplate his own beauty. He was so
fascinated by himself that, one morning, he fell into the lake and
drowned. At the spot where he fell, a flower was born, which was
called the narcissus.
{'\n'}{'\n'}
But this was not how the author of the book ended the story.
He said that when Narcissus died, the goddesses of the forest
appeared and found the lake, which had been fresh water,
transformed into a lake of salty tears.
{'\n'}{'\n'}
“Why do you weep?” the goddesses asked.
{'\n'}{'\n'}
“I weep for Narcissus,” the lake replied.
{'\n'}{'\n'}
“Ah, it is no surprise that you weep for Narcissus,” they said, “for
though we always pursued him in the forest, you alone could
contemplate his beauty close at hand.”
{'\n'}{'\n'}
“But…was Narcissus beautiful?” the lake asked.
{'\n'}{'\n'}
“Who better than you to know that?” the goddesses said in
wonder. “After all, it was by your banks that he knelt each day to
contemplate himself!”
{'\n'}{'\n'}
The lake was silent for some time. Finally, it said:
{'\n'}{'\n'}
“I weep for Narcissus, but I never noticed that Narcissus was
beautiful. I weep because, each time he knelt beside my banks, I
could see, in the depths of his eyes, my own beauty reflected.”
{'\n'}{'\n'}
“What a lovely story,” the alchemist thought.

</Text>
    </ScrollView>
    </SafeAreaView>
);
};

  export default Dbook1;