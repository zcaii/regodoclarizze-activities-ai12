import React from 'react-native';
import { Text, View, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

const styles = StyleSheet.create({
  btn: {
    paddingTop: 10,
    marginLeft: 5
  },
    pressButton: {
      color: "#000000",
      fontWeight: "bold",
      alignItems: 'center',
      justifyContent: 'center',
      fontSize: 20,
      marginLeft: 10,
      marginBottom: 0
      },
  text: {
    textAlign: 'justify',
    marginTop: 10,
    fontSize: 18,
    marginLeft: 10,
    marginRight: 10,
    color: "#000000"
  },
  page: {
    fontWeight: "bold",
    marginTop: 10,
    fontSize: 25,
    marginLeft: 10,
    marginRight: 10,
    color: "#000000",
  },
  context: {
    top: 10,
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center",
    color: "#857171"
  },
  view: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
  },
  title: {
  marginTop: 1,
  marginLeft: 10
  },
  back: {
    flex: 1,
    justifyContent: "center",
  },
});


const Dbook4 = ({ navigation }) => {
    return (
      <SafeAreaView style={styles.container}>
      <Text style={styles.page}>Epilogue</Text>

      <View style={styles.btn}>
        <TouchableOpacity onPress={() => navigation.navigate("AlchemistChapters")}>
          <View style={styles.press}>
            <Text style={styles.pressButton}>Go back</Text>
          </View>
        </TouchableOpacity>
      </View>

      <ScrollView style={styles.scrollView}>
        <Text style={styles.text}>
        THE BOY REACHED THE SMALL, ABANDONED CHURCH JUST as night was
falling. The sycamore was still there in the sacristy, and the stars
could still be seen through the half-destroyed roof. He remembered
the time he had been there with his sheep; it had been a peaceful
night…except for the dream.
{'\n'}{'\n'}
Now he was here not with his flock, but with a shovel.
He sat looking at the sky for a long time. Then he took from his
knapsack a bottle of wine, and drank some. He remembered the
night in the desert when he had sat with the alchemist, as they
looked at the stars and drank wine together. He thought of the many
roads he had traveled, and of the strange way God had chosen to
show him his treasure. If he hadn’t believed in the significance of
recurrent dreams, he would not have met the Gypsy woman, the
king, the thief, or…“Well, it’s a long list. But the path was written in
the omens, and there was no way I could go wrong,” he said to
himself.
{'\n'}{'\n'}
He fell asleep, and when he awoke the sun was already high. He
began to dig at the base of the sycamore.
“You old sorcerer,” the boy shouted up to the sky. “You knew the
whole story. You even left a bit of gold at the monastery so I could
get back to this church. The monk laughed when he saw me come
back in tatters. Couldn’t you have saved me from that?”
“No,” he heard a voice on the wind say. “If I had told you, you
wouldn’t have seen the Pyramids. They’re beautiful, aren’t they?”
The boy smiled, and continued digging. Half an hour later, his
shovel hit something solid. An hour later, he had before him a chest
of Spanish gold coins. There were also precious stones, gold masks
adorned with red and white feathers, and stone statues embedded
with jewels. The spoils of a conquest that the country had long ago
forgotten, and that some conquistador had failed to tell his children
about.
{'\n'}{'\n'}
The boy took out Urim and Thummim from his bag. He had used
the two stones only once, one morning when he was at a
marketplace. His life and his path had always provided him with
enough omens.
{'\n'}{'\n'}
He placed Urim and Thummim in the chest. They were also a
part of his new treasure, because they were a reminder of the old
king, whom he would never see again.
{'\n'}{'\n'}
It’s true; life really is generous to those who pursue their
Personal Legend, the boy thought. Then he remembered that he had
to get to Tarifa so he could give one-tenth of his treasure to the
Gypsy woman, as he had promised. Those Gypsies are really smart,
he thought. Maybe it was because they moved around so much.
The wind began to blow again. It was the levanter, the wind that
came from Africa. It didn’t bring with it the smell of the desert, nor
the threat of Moorish invasion. Instead, it brought the scent of a
perfume he knew well, and the touch of a kiss—a kiss that came
from far away, slowly, slowly, until it rested on his lips.
The boy smiled. It was the first time she had done that.
“I’m coming, Fatima,” he said.


</Text>
    </ScrollView>
    </SafeAreaView>
);
};

  export default Dbook4;