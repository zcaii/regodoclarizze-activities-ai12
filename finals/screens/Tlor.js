import React from 'react-native';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btn:{
    height:30,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  pressButton: {
    color: "#edc405",
    fontWeight: "bold",
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 25
  },
  text: {
    top: 10,
    width: 400,
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center"
  },
  page: {
    fontWeight: "bold",
    marginTop: 10,
    fontSize: 100,
    marginLeft: 50,
    marginRight: 50,
    color: "#fff"
  },
  view: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white"
  },
  title: {
    marginTop: 10,
    marginLeft: 20
  },
  back: {
    flex: 1,
    justifyContent: "center",
  },
});


const Tlor = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Image source= {require("../background/image2.jpg")}
          style={{width: 400, height: 600, marginTop: 0, marginLeft: 40, marginRight: 40,}}/>
      <View style={styles.btn}>
        <TouchableOpacity onPress={() => navigation.navigate("CHAPTERS")}>
          <View style={styles.press}>
            <Text style={styles.pressButton}>CHAPTERS</Text>
          </View>
        </TouchableOpacity>
      </View>

      <View style={styles.btn}>
          <TouchableOpacity onPress={() => navigation.goBack("Books")}>
            <View style={styles.press}>
              <Text style={styles.pressButton}>Books</Text>
            </View>
          </TouchableOpacity>
        </View>

      
      <View style={styles.btn}>
        <TouchableOpacity onPress={() => navigation.navigate("Home Screen")}>
          <View style={styles.press}>
            <Text style={styles.pressButton}>Home</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Tlor;