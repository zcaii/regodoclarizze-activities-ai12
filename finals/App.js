import { createStackNavigator } from '@react-navigation/stack';
import {
  Text,
  View,
  StyleSheet,
  Image } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { TouchableOpacity } from 'react-native-gesture-handler';

import MangaComics from './components/MangaComics';
import MyWeb from './components/MyWeb';
import Books from './components/Books';

import Tlor from './screens/Tlor';
import ChapterScreen from './components/ChapterScreen';
import Chapters from './screens/Chapters';
import Chapter2 from './screens/Chapter2';
import Chapter3 from './screens/Chapter3';
import Chapter5 from './screens/Chapter5';
import Chapter4 from './screens/Chapter4';
import Chapter6 from './screens/Chapter6';
import Chapter7 from './screens/Chapter7';
import Chapter8 from './screens/Chapter8';
import Chapter9 from './screens/Chapter9';
import Chapter10 from './screens/Chapter10';
import Chapter11 from './screens/Chapter11';
import Chapter12 from './screens/Chapter12';

import Rfalls from './components/Rfalls';
import Harrypotter from './components/Harrypotter';
import Book1 from './screens/Book1';
import Book2 from './screens/Book2';
import Book3 from './screens/Book3';
import Book4 from './screens/Book4';
import Book5 from './screens/Book5';
import Book6 from './screens/Book6';
import Book7 from './screens/Book7';
import Book8 from './screens/Book8';
import Book9 from './screens/Book9';
import Book10 from './screens/Book10';

import TheAlchemist from './components/TheAlchemist';
import AlchemistChapters from './components/AlchemistChapters';
import Dbook1 from './screens/Dbook1';
import Dbook2 from './screens/Dbook2';
import Dbook3 from './screens/Dbook3';
import Dbook4 from './screens/Dbook4';

import TheCallchapters from './components/TheCallchapters';
import TheCallOFTheWind from './components/TheCallOfTheWind';
import Ebook1 from './screens/Ebook1';
import Ebook2 from './screens/Ebook2';
import Ebook3 from './screens/Ebook3';
import Ebook4 from './screens/Ebook4';
import Ebook5 from './screens/Ebook5';
import Ebook6 from './screens/Ebook6';
import Ebook7 from './screens/Ebook7';



const styles = StyleSheet.create({
  pressButton: {
    color: '#f0d77a',
    fontWeight: 'bold',
    fontSize: 50
  },
  text: {
    top: 50,
    width: 400,
    fontSize: 50,
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center"
  },
  view: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#000000",
  },
  back: {
    flex: 1,
    justifyContent: "center",
  },
  NavigationContainer: {
    backgroundColor: "#0000FF",
    padding: 20,
    justifyContent: 'space-around',
    flexDirection: 'row'
  },
  container: {
    flex: 1,
    backgroundColor: '#000000',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

//Front Page
const Homepage = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Image source={require("./background/Frontpage.png")}
        style={{width: 400, height: 500, marginBottom:10}}/>
      <View style={styles.press}>
        <TouchableOpacity onPress={() => navigation.navigate("Books")}>
          <View style={styles.press}>
            <Text style={styles.pressButton}>Books</Text>
          </View>
        </TouchableOpacity>
      </View>

      <View style={styles.press}>
        <TouchableOpacity onPress={() => navigation.navigate("MyWeb")}>
          <View style={styles.press}>
            <Text style={styles.pressButton}>WebNovel</Text>
          </View>
        </TouchableOpacity>
      </View>

      <View style={styles.press}>
        <TouchableOpacity onPress={() => navigation.navigate("MangaComics")}>
          <View style={styles.press}>
            <Text style={styles.pressButton}>Manga</Text>
          </View>
        </TouchableOpacity>
      </View>

    </View>
  )
}

const Stack = createStackNavigator();
export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{ headerShown: false }}>
        <Stack.Screen name="Home Screen" component={Homepage} />
        <Stack.Screen name="Books" component={Books}/>
        <Stack.Screen name="MyWeb" component={MyWeb} />
        <Stack.Screen name="MangaComics" component={MangaComics} />

        <Stack.Screen name="The Lord of the Ring" component={Tlor} />
        <Stack.Screen name="CHAPTERS" component={ChapterScreen} />
        <Stack.Screen name="C1" component={Chapters} />
        <Stack.Screen name="C2" component={Chapter2} />
        <Stack.Screen name="C3" component={Chapter3} />
        <Stack.Screen name="C4" component={Chapter4} />
        <Stack.Screen name="C5" component={Chapter5} />
        <Stack.Screen name="C6" component={Chapter6} />
        <Stack.Screen name="C7" component={Chapter7} />
        <Stack.Screen name="C8" component={Chapter8} />
        <Stack.Screen name="C9" component={Chapter9} />
        <Stack.Screen name="C10" component={Chapter10} />
        <Stack.Screen name="C11" component={Chapter11} />
        <Stack.Screen name="C12" component={Chapter12} />

        <Stack.Screen name="Harry Potter" component={Rfalls} />
        <Stack.Screen name="Chapters" component={Harrypotter} />
        <Stack.Screen name="B1" component={Book1} />
        <Stack.Screen name="B2" component={Book2} />
        <Stack.Screen name="B3" component={Book3} />
        <Stack.Screen name="B4" component={Book4} />
        <Stack.Screen name="B5" component={Book5} />
        <Stack.Screen name="B6" component={Book6} />
        <Stack.Screen name="B7" component={Book7} />
        <Stack.Screen name="B8" component={Book8} />
        <Stack.Screen name="B9" component={Book9} />
        <Stack.Screen name="B10" component={Book10} />

        <Stack.Screen name="The Alchemist" component={TheAlchemist} />
        <Stack.Screen name="AlchemistChapters" component={AlchemistChapters} />
        <Stack.Screen name="D1" component={Dbook1} />
        <Stack.Screen name="D2" component={Dbook2} />
        <Stack.Screen name="D3" component={Dbook3} />
        <Stack.Screen name="D4" component={Dbook4} />

        <Stack.Screen name="The Call of the Wind" component={TheCallOFTheWind} />
        <Stack.Screen name="ThCallchapters" component={TheCallchapters} />
        <Stack.Screen name="E1" component={Ebook1} />
        <Stack.Screen name="E2" component={Ebook2} />
        <Stack.Screen name="E3" component={Ebook3} />
        <Stack.Screen name="E4" component={Ebook4} />
        <Stack.Screen name="E5" component={Ebook5} />
        <Stack.Screen name="E6" component={Ebook6} />
        <Stack.Screen name="E7" component={Ebook7} />


      </Stack.Navigator>
    </NavigationContainer>
  );
}
