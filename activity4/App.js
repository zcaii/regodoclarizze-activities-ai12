import { StyleSheet, Text, View, ImageBackground } from "react-native";
import Ionic from "react-native-vector-icons/Ionicons";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

 
const styles = StyleSheet.create({
  buttons: {
    paddingTop: 150,
  },
  context: {
    marginTop: 180,
    fontSize: 18,
    marginLeft: 40,
    marginRight: 30,
    color: "#5e493d",

  },
  text: {
  top: 200,
  width: 300,
  alignContent: "center",
  alignItems: "center",
  justifyContent: "center"
  },
  page: {
    fontWeight: "bold",
    fontSize: 30,
    color: "#5e493d",
  },
  view: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
  },
  back: {
    flex: 1,
    justifyContent: "center",
  },
});

//Home Page
function HomeScreen({ navigation }) {
  return (
    <View style={styles.view}>
      <ImageBackground
        source={require("./background/screen1.png")}
        resizeMode="contain"
        style={styles.back}
      >
        <Text style={styles.page}>Reasons Why I Chose IT Course</Text>
        <Text style={styles.context}>
        </Text>
        <Ionic
          style={styles.buttons}
          name="chevron-forward-circle"
          size={40}
          color={"#5e493d"}
          onPress={() => navigation.navigate("Reason 1")}
        ></Ionic>
      </ImageBackground>
    </View>
  );
}

//1st Page
function Reason1({ navigation }) {
  return (
    <View style={styles.view}>
      <ImageBackground
        source={require("./background/screen2.png")}
        resizeMode="contain"
        style={styles.back}
      >
        <Text style={styles.page}>Why IT?</Text>
        <View style={styles.text}>
        <Text style={styles.title}>
        </Text>
        <Text style={styles.context}>
                IT was not in my choice list until
                my Mother encouraged me to take LNU admission test
                for BSIT. It was hard, but
                I found myself enjoying the whole day coding
                especially if I understand the code.
          </Text>
          </View>
        <Ionic
          style={styles.buttons}
          name="chevron-forward-circle"
          size={40}
          color={"#5e493d"}
          onPress={() => navigation.navigate("Reason 2")}
        ></Ionic>
      </ImageBackground>
    </View>
  );
}
//Second Page
function Reason2({ navigation }) {
  return (
    <View style={styles.view}>
      <ImageBackground
        source={require("./background/screen3.png")}
        resizeMode="contain"
        style={styles.back}
      >
        <Text style={styles.page}>Reason 2</Text>
        <View style={styles.text}>
        <Text style={styles.title}>
        </Text>
        <Text style={styles.context}>
         As an IT student, I felt like a cool person
         who write codes even though I'm not good at it.
         </Text>
         </View>
         <Ionic
          style={styles.buttons}
          name="chevron-forward-circle"
          size={40}
          color={"#5e493d"}
          onPress={() => navigation.navigate("Reason 3")}
        ></Ionic>
      </ImageBackground>
    </View>
  );
}

//Third Page
function Reason3({ navigation }) {
  return (
    <View style={styles.view}>
      <ImageBackground
        source={require("./background/screen4.png")}
        resizeMode="contain"
        style={styles.back}
      >
       <Text style={styles.page}>Reason 3</Text>
        <View style={styles.text}>
        <Text style={styles.title}>
        </Text>
        <Text style={styles.context}>
          If I see apps or any investions related to technology,
          I always imagined myself doing some investions too and it's
          kinda funny because technology never fails to astonish me.
        </Text>
        </View>
        <Ionic
          style={styles.buttons}
          name="chevron-forward-circle"
          size={40}
          color={"#5e493d"}
          onPress={() => navigation.navigate("Reason 4")}
          
        ></Ionic>
      </ImageBackground>
    </View>
  );
}

//Fourth Page
function Reason4({ navigation }) {
  return (
    <View style={styles.view}>
      <ImageBackground
        source={require("./background/screen5.png")}
        resizeMode="contain"
        style={styles.back}
      >
       <Text style={styles.page}>Reason 4</Text>
        <View style={styles.text}>
        <Text style={styles.title}>
        </Text>
        <Text style={styles.context}>
         It develops new things and its sounds pretty cool.
        </Text>
        </View>
        <Ionic
          style={styles.buttons}
          name="chevron-forward-circle"
          size={40}
          color={"#5e493d"}
          onPress={() => navigation.navigate("Reason 5")}
        ></Ionic>
      </ImageBackground>
    </View>
  );
}
//Fifth Page
function Reason5({ navigation }) {
  return (
    <View style={styles.view}>
      <ImageBackground
        source={require("./background/screen6.png")}
        resizeMode="contain"
        style={styles.back}
      >
        <Text style={styles.page}>Reason 5</Text>
        <View style={styles.text}>
        <Text style={styles.title}>
        </Text>
        <Text style={styles.context}>
         In my experiences as of now, I am still in the point
         where I have to know more about programming.
         It was like a baby step to me where I should
         start from the very beginning. I have will spend and dedicate my
         time in coding just to get the exact output I am aiming for.
         Being an IT student is so challenging more likely your life is
         a trial and error.
        </Text>
        </View>
        <Ionic
          style={styles.buttons}
          name="ios-home-outline"
          size={30}
          color={"#5e493d"}
          onPress={() => navigation.navigate("Reason 1")}
        ></Ionic>
      </ImageBackground>
    </View>
  );
}

const Stack = createStackNavigator();
export default function App({}) {
  return (
    <NavigationContainer>
      <Stack.Navigator    
        initialRouteName="Home"
      >
        <Stack.Screen name="Reasons Why I Chose IT Course" component={HomeScreen} />
        <Stack.Screen name="Reason 1" component={Reason1} />
        <Stack.Screen name="Reason 2" component={Reason2} />
        <Stack.Screen name="Reason 3" component={Reason3} />
        <Stack.Screen name="Reason 4" component={Reason4} />
        <Stack.Screen name="Reason 5" component={Reason5} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}