import React from "react";
import { StatusBar } from "react-native";
import Ionic from "react-native-vector-icons/Ionicons";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import activity2 from "./screens/activity2";
import ProfileScreen from "./screens/ProfileScreen";

const App = () => {
  const Tab = createBottomTabNavigator();
  return (
    <>
      <StatusBar backgroundColor="#eba459" barStyle="dark-content" />
      <NavigationContainer>
        <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, size, colour }) => {
              let iconName;
              if (route.name === "Todolist") {
                iconName = focused ? "home" : "list-circle-outline";
                size = focused ? size + 8 : size + 5;
              } else if (route.name === "Completed Tasks") {
                iconName = focused ? "trophy" : "list-outline";
                size = focused ? size + 8 : size + 5;
              }
              return <Ionic name={iconName} size={size} colour={colour} />;
            },
          })}
          tabBarOptions={{
            activeTintColor: "black",
            inactiveTintColor: "black",
            showLabel: false,
            tabStyle: {
              backgroundColor: "#eba459",
              height: 60,
            },
          }}
        >
          <Tab.Screen name="Todolist" component={activity2} />
          <Tab.Screen name="Completed Tasks" component={ProfileScreen} />
        </Tab.Navigator>
      </NavigationContainer>
    </>
  );
};
export default App;
