import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { CheckBox, TouchableOpacity } from "react-native-web";

const Task = (props) => {
    return (
        <View style={styles.item}>
            <View style={styles.itemLeft}>
                <View style={styles.circle}></View>
                <Text style={styles.itemText}>{props.text}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    item: {
        backgroundColor: '#fcf2e3',
        padding: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10,
        paddingVertical: 50,
    },
    itemLeft: {
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    circle: {
        width: 27,
        height: 27,
        backgroundColor: '#b50073',
        borderWidth: 3,
        borderColor: '#600063',
        borderRadius: 60,
        marginRight: 15,
    },
    itemText: {
        maxWidth: '85%',
        color: '#600063',
        fontWeight: 'bold',
        fontSize: 20,
    },
});
export default Task;