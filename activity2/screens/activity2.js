import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Keyboard, ScrollView } from 'react-native';
import Task from '../components/Task';

export default function App() {
    const [task, setTask] = useState();
    const [taskItems, setTaskItems] = useState([]);
    const handleAddTask = () => {
        Keyboard.dismiss();
        setTaskItems([...taskItems, task])
        setTask("");
    }
    const completeTask = (index) => {
        let itemsCopy = [...taskItems];
        itemsCopy.splice(index, 1);
        setTaskItems(itemsCopy)
    }
    return (
        <View style={styles.container}>
            <TextInput style={styles.text_Field} placeholder={'Write a task'} value={task} onChangeText={text => setTask(text)} />
            <View style={styles.btn_component}>
                <TouchableOpacity onPress={() => handleAddTask()}>
                    <Text style={styles.btn_Add}>Add Task</Text>
                </TouchableOpacity>
            </View>
            <ScrollView style={styles.scroll_component} contentContainerStyle={{ flexGrow: 1 }} keyboardShouldPersistTaps='handled'>
                <View style={styles.items}>
                    {
                        taskItems.map((item, index) => {
                            return (
                                <TouchableOpacity key={index} onPress={() => completeTask(index)}>
                                    <Task text={item} />
                                </TouchableOpacity>
                            )
                        })
                    }
                </View>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        marginTop: 60,
        flex: 1,
        position: 'relative',
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center',
},

    scroll_component: {
        marginTop: 20,
        width: '90%',
    },
    text_Field: {
        paddingVertical: 15,
        paddingHorizontal: 15,
        backgroundColor: '#fcf2e3',
        borderColor: '#600063',
        borderWidth: 2,
        borderRadius: 6,
        width: '90%',
    },
    btn_component: {
        marginTop: 20,
        paddingVertical: 10,
        paddingHorizontal: 15,
        backgroundColor: '#e6b163',
        borderColor: '#600063',
        borderWidth: 2,
        borderRadius: 10,
        width: '90%',
    },
    btn_Add: {
        paddingHorizontal: 15,
        backgroundColor: '#e6b163',
        textAlign: 'center',
        color: '#600063',
        fontWeight: 'bold',
    },
});
