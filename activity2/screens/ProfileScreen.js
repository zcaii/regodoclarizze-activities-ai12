import { StyleSheet, Text, View } from "react-native";

export default function ProfileScreen() {
    return (
        <View 
        style={{ 
          height: "90%",
          backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center",
        }}
        >
            <Text style={{ fontsize: 40,
                fontWeight:"bold",
                letterSpacing: 4, 
                color: "#600063"}}>
                WELL DONE! COMPLETED TASKS WILL GO HERE!
            </Text>
        </View>
      );
    }
    
    